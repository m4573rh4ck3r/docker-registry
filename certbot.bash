#!/bin/bash

docker run \
        -p 80:80 \
        -p 443:443 \
        -it \
        --rm \
        --name certbot \
        -v "/etc/letsencrypt:/etc/letsencrypt" \
         -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
        certbot/certbot renew

cp /etc/letsencrypt/archive/registry.h4ck3r.cc/fullchain1.pem /root/.certs/domain.crt
cp /etc/letsencrypt/archive/registry.h4ck3r.cc/privkey1.pem /root/.certs/domain.key
